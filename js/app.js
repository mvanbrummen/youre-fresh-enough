var app = angular.module("sweetApp", ["angularModalService"]);

app.controller("sweetController", function($scope, ModalService) {
  $scope.showInfoModal = function(img, info, large) {
    var style = "";
    if (large) {
      style = 'style="height: 500px;width: 800px;background-color: #af1764"';
    }
    ModalService.showModal({
      template: '<div class="modal fade"> <div class="modal-dialog"> <div class="modal-content"' + style + ' > <div class="modal-body"> <img src="img/' + img + '"></img> ' + info + ' </div> </div> </div> </div> ',
      controller: "modalController"
    }).then(function(modal) {
      modal.element.modal();
    });
  };
});

app.controller("modalController", function($scope, close) {
  $scope.close = function(result) {
  	close(result, 500); // close, but give 500ms for bootstrap to animate
  };
});
